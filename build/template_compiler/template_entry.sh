#!/bin/bash
#
# Purpose: common script to be added to the entrypoint or command for Docker containers using the template_compiler
#          program for compiling templates.
#
# Usage: simply mount the template_compiler directory to root in your Docker image and add:
#        ./template_compiler/template_entry.sh
#        inside your entrypoint or command script prior to executing the container's main process.
#

# compile templates
python /template_compiler/compile_templates.py