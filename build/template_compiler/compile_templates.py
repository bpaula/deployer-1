# Compile templates listed in /templates using values supplied in a yaml file.
# May, 2015.

# Usage:
# read /values.yml by default:
# python compile_templates.py

# supply a specific values file:
# python compile_templates.py /path/to/values.yml
#

import argparse
import jinja2
import os
import sys
import time
import yaml

class ConfigGen(object):
    """
    Utility class for generating a config file from a jinja template.
    """
    def __init__(self, template_str):
        self.template_str = template_str

    def compile(self, configs, env):
        template = env.get_template(self.template_str)
        return template.render(configs)

    def generate_conf(self, configs, path, env):
        output = self.compile(configs, env)
        with open(path, 'w+') as f:
            f.write(output)

def file_to_dict(path):
    return yaml.load(open(path)) or {}

def render_template(line, context):
    conf = ConfigGen(line)
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('/'), trim_blocks=True, lstrip_blocks=True)
    conf.generate_conf(context, line, env)
    print "Compiled template ", line

def process_userstore(line, context):
    """APIM userstore templates need special handling at runtime since some may not be needed and those that are need
    to be renamed.
    """
    render = False
    if 'hosted_id_domain_name.xml' in line:
        render = context.get('use_hosted_id')
        if render:
            name = context.get('hosted_id_domain_name')
            if not name:
                print "ERROR - unable to generate hosted id userstore; hosted_id_domain_name not configured"
                sys.exit(1)
            path = line.replace('hosted_id_domain_name', name)
    elif 'remote_domain_name.xml' in line:
        render = context.get('use_remote_userstore')
        if render:
            name = context.get('remote_id_domain_name')
            if not name:
                print "ERROR - unable to generate remote userstore; remote_id_domain_name not configured"
                sys.exit(1)
            path = line.replace('remote_domain_name', name)
    if render:
        conf = ConfigGen(line)
        env = jinja2.Environment(loader=jinja2.FileSystemLoader('/'), trim_blocks=True, lstrip_blocks=True)
        conf.generate_conf(context, path, env)
        print "Compiled template ", line, "to ", path
    os.remove(line)
    print "Removed template: ", line

def compile_templates(values_file):
    print "Compiling templates..."
    context = file_to_dict(values_file)
    context.update(file_to_dict('/passwords'))
    with open('/templates', 'r') as f:
        for line in f.readlines():
            line = line.strip('\n')
            if not os.path.exists(line):
                continue
            if 'hosted_id_domain_name.xml' in line or 'remote_domain_name.xml' in line:
                process_userstore(line, context)
            else:
                render_template(line, context)

def main():
    values_file = '/values.yml'
    parser = argparse.ArgumentParser(description='Compile templates listed in /templates with a values.yml file.')
    parser.add_argument('-v', '--values_file', type=str, help='Yaml file containing values for template strings.')
    args = parser.parse_args()
    if args.values_file:
        values_file = args.values_file
    compile_templates(values_file)
    print "Finished compiling templates."


if __name__ == '__main__':
    main()