===================
Deployer Validation
===================

Deployer validation is performed by creating four VMs, a VM to manage creation of VMs and to perform tests, and three additional VMs to host the Agave services, an Auth VM, a core VM, and a database VM. The validation functionality is implemented in an Ansible playbook, ``validate_deployer.plbk``.

Components
==========

Accounts
--------

Host Files
----------

The ``validate_deployer.plbk`` relies on one 

Roles
-----

Configuration Files
-------------------
