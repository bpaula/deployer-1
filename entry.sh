#!/bin/bash

export ANSIBLE_HOST_KEY_CHECKING=False

## this line not needed as long as ansible is bundled with deployer
# cd /ansible; git checkout -b origin/stable-2.0.0.1; git submodule update --init --recursive

source /ansible/hacking/env-setup

ansible-playbook $*
